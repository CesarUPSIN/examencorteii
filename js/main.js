var totalGeneral = 0.0;

function eventoCambio() {
    let cmbMoneda1 = document.getElementById('cmbMoneda1').value;
    let cmbMoneda2 = document.getElementById('cmbMoneda2');

    if(cmbMoneda1 == 1) {
        cmbMoneda2.options[0] = new Option('Dolar estadounidense', 0);
        cmbMoneda2.options[1] = new Option('Dolar canadiense', 1);
        cmbMoneda2.options[2] = new Option('Euro', 2);
    }
    if(cmbMoneda1 == 2) {
        cmbMoneda2.options[0] = new Option('Peso mexicano', 0);
        cmbMoneda2.options[1] = new Option('Dolar canadiense',1);
        cmbMoneda2.options[2] = new Option('Euro', 2);
    }
    if(cmbMoneda1 == 3) {
        cmbMoneda2.options[0] = new Option('Peso mexicano', 0);
        cmbMoneda2.options[1] = new Option('Dolar estadounidense', 1);
        cmbMoneda2.options[2] = new Option('Euro', 2);
    }
    if(cmbMoneda1 == 4) {
        cmbMoneda2.options[0] = new Option('Peso mexicano', 0);
        cmbMoneda2.options[1] = new Option('Dolar canadiense', 1);
        cmbMoneda2.options[2] = new Option('Dolar estadounidense', 2);
    }
}

function calcular() {
    let cantidad = document.getElementById('txtCantidad').value;
    let origen = document.getElementById('cmbMoneda1');
    let destino = document.getElementById('cmbMoneda2');
    let subtotal = document.getElementById('txtSubtotal');
    let totalComision = document.getElementById('txtTotalComision');
    let totalPagar = document.getElementById('txtTotalPagar');
    let tasa = [19.85, 1, 1.35, .99];

    subtotal.value = ((cantidad / tasa[origen.value-1]) * (tasa[parseInt(destino.value)+1])).toFixed(2);
    totalComision.value = (subtotal.value * .03).toFixed(2);
    totalPagar.value = (parseFloat(subtotal.value) + parseFloat(totalComision.value)).toFixed(2);
}

function registro() {
    let cantidad = document.getElementById('txtCantidad').value;
    let origen = document.getElementById('cmbMoneda1');
    let destino = document.getElementById('cmbMoneda2');
    let registros = document.getElementById('registros');
    let subtotal = document.getElementById('txtSubtotal');
    let totalComision = document.getElementById('txtTotalComision');
    let totalPagar = document.getElementById('txtTotalPagar');
    let txtTotalGeneral = document.getElementById('txtTotalGeneral');

    registros.innerHTML = registros.innerHTML + cantidad + ' '+ origen.options[origen.selectedIndex].text + ' a ' + destino.options[destino.selectedIndex].text + ' ' + subtotal.value + ' ' + totalComision.value + ' ' + totalPagar.value + '<br>';
    totalGeneral = parseFloat(totalGeneral) + parseFloat(totalPagar.value);
    txtTotalGeneral.innerHTML = parseFloat(totalGeneral);
}

function borrar() {
    let registros = document.getElementById('registros');
    let txtTotalGeneral = document.getElementById('txtTotalGeneral');

    registros.innerHTML = ' ';
    txtTotalGeneral.innerHTML = ' ';
}